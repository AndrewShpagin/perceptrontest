#pragma once

//-----------------------------------------------------------------------------
// cThread
//-----------------------------------------------------------------------------
namespace cThread {
	
typedef void (*ThreadProc)(void *);

#if defined COMMS_WINDOWS
typedef HANDLE ThreadHandle;
typedef HANDLE ThreadMutex;
#endif // COMMS_WINDOWS

#if defined COMMS_MACOS || defined COMMS_LINUX || defined COMMS_IOS || defined COMMS_TIZEN
#include <pthread.h>
typedef pthread_t ThreadHandle;
typedef pthread_mutex_t ThreadMutex;
#endif // COMMS_MACOS || COMMS_LINUX || COMMS_IOS || COMMS_TIZEN

int CpuCount();
void Sleep(const dword Milliseconds);

ThreadHandle CreateThread(ThreadProc Proc, void *Param, const bool Critical = false, const int Index = 0);
void DeleteThread(ThreadHandle *Thread);
void WaitAndDeleteThread(ThreadHandle *Thread);
void CancelAndDeleteThread(ThreadHandle *Thread);

void CreateMutex(ThreadMutex &Mutex);
void DeleteMutex(ThreadMutex &Mutex);
void LockMutex(ThreadMutex &Mutex);
void UnlockMutex(ThreadMutex &Mutex);

}; // cThread
