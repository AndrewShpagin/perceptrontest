#include "comms-Math.h"

namespace comms {

static cThread::ThreadMutex COMMS_POOL_MUTEX;
static bool COMMS_POOL_MUTEX_CREATED = false;

// cPool::Alloc
byte * cPool::Alloc(const int Size) {
    if(Size <= 0) {
        return NULL;
    }
    if(!COMMS_POOL_MUTEX_CREATED) {
        cThread::CreateMutex(COMMS_POOL_MUTEX);
        COMMS_POOL_MUTEX_CREATED = true;
    }
    cThread::LockMutex(COMMS_POOL_MUTEX);
    byte *r = NULL;
    cPool_Store *P = GetPoolBySize(Size);
    if(P != NULL) {
        r = P->Allocate();
    }
    cThread::UnlockMutex(COMMS_POOL_MUTEX);
    if(NULL == r) {
        r = new byte[Size];
    }
    return r;
}

// cPool::Free
void cPool::Free(byte *Ptr, const int Size) {
    if(!COMMS_POOL_MUTEX_CREATED) {
        cThread::CreateMutex(COMMS_POOL_MUTEX);
        COMMS_POOL_MUTEX_CREATED = true;
    }
    cThread::LockMutex(COMMS_POOL_MUTEX);
    cPool_Store *P = GetPoolBySize(Size);
    if(P != NULL) {
        P->Free(Ptr);
        Ptr = NULL;
    }
	cThread::UnlockMutex(COMMS_POOL_MUTEX);
    if(Ptr != NULL) {
        delete[] Ptr;
    }
}

cPool_Store * COMMS_POOL[5] = { // 8, 16, 32, 64, 128
	NULL, NULL, NULL, NULL, NULL
};

int cPool_Debug::UsedCount = 0;

} // comms
