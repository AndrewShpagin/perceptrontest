#pragma once
//macros set to organize parallel calculations

#ifdef PARALLEL_CALC
//1. for - like cycle
//tbb_for(i,N){
//   ...body of cycle...
//}tbb_for_end

#define tbb_for(variable,count) tbb::parallel_for(0,count,[&](variable)
#define tbb_for_end ); 

#else //PARALLEL_CALC

#define tbb_for(variable,count) for(int i=0;i<count;i++)
#define tbb_for_end 

#endif
