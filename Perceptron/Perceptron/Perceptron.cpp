// CommsTonsole.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include "Perceptron.h"
#include "tbb/include/tbb.h"

#define PARALLEL_CALC

#include "parallel.h"

const float sgspeed = 1.0f;
float Sigma(float x){
	return 1.0f / (1.0f + exp(-x*sgspeed));
}
float SigmaDerivative(float x){
	float ex = exp(-x*sgspeed);
	float ex1 = 1.0f + ex;
	ex1 *= ex1;
	return sgspeed*ex / ex1;
}

float Sigma1(float x){
	return x > 0 ? 1.0f - 1.0f / (2.0f + x) : 1.0f / (2.0f - x);
}
float SigmaDerivative1(float x){
	return x > 0 ? 1.0f / (2.0f + x) / (2.0f + x) : 1.0f / (2.0f - x) / (2.0f - x);
}

float rand1(){
	return rand() / 16384.0f - 1.0f;
}
void PerceptronLayer::Create(int _NInput, int _NPerceptrons, bool Random){
	NInput = _NInput;
	NPerceptrons = _NPerceptrons;
	int sz = NInput*NPerceptrons;
	float v = 0;
	WeightsMatrix.SetCount(sz, v);
	Biases.SetCount(NPerceptrons, v);
	if (Random){
		for (int i = 0; i < sz; i++){
			WeightsMatrix[i] = rand1();
		}
		for (int i = 0; i < NPerceptrons; i++){
			Biases[i] = rand1();
		}
	}
}
void PerceptronLayer::Mul(const fvector& in, fvector& out) const{
	const float* m = WeightsMatrix.ToPtr();
	const float* pin = in.ToPtr();
	float* pout = out.ToPtr();
	for (int y = 0; y < NPerceptrons; y++){
		float s = 0;
		for (int x = 0; x < NInput; x++){
			s += (*(m++)) * (*(pin++));
		}
		pin -= NInput;
		*(pout++) = s;
	}
}
void PerceptronLayer::MulSig(const fvector& in, fvector& out, fvector* z) const{
	const float* m = WeightsMatrix.ToPtr();
	const float* pin = in.ToPtr();
	const float* bet = Biases.ToPtr();
	float* pout = out.ToPtr();
	float* pz = z ? z->ToPtr() : NULL;
	for (int y = 0; y < NPerceptrons; y++){
		float s = 0;
		for (int x = 0; x < NInput; x++){
			s += (*(m++)) * (*(pin++));
		}
		pin -= NInput;
		s += *(bet++);
		if (z){
			*(pz++) = s;
		}
		*(pout++) = Sigma(s);
	}
}
void PerceptronLayer::MulT(const fvector& in, fvector& out) const{
	const float* m = WeightsMatrix.ToPtr();
	const float* pin = in.ToPtr();
	float* pout = out.ToPtr();
	const float* m0 = m;
	for (int y = 0; y < NInput; y++){
		float s = 0;
		m = m0;
		for (int x = 0; x < NPerceptrons; x++){
			s += (*m) * (*(pin++));
			m += NInput;
		}
		m0++;
		pin -= NPerceptrons;
		*(pout++) = s;
	}
}
void PerceptronLayer::Calculate(fvector& in, fvector& outp,fvector* z) const {
	MulSig(in, outp,z);
}
void PerceptronLayer::random(){
	int sz = NPerceptrons*NInput;
	for (int i = 0; i < sz; i++){
		WeightsMatrix[i] = rand1();
	}
	for (int i = 0; i < NPerceptrons; i++){
		Biases[i] = rand1();
	}
}

PerceptronsLayers::PerceptronsLayers(){
	NInput = 1;
}
PerceptronsLayers::PerceptronsLayers(int nin){
	NInput = nin;
}
void PerceptronsLayers::random(){
	for (int i = 0; i < Matrix.Count(); i++){
		Matrix[i].random();
	}
}
void PerceptronTeacher::CreateFields(const PerceptronsLayers& mat){
	mat.CreateField(a);
	mat.CreateField(z, true);
	mat.CreateField(delta, true);
}
void PerceptronTeacher::Backpropagation(const PerceptronsLayers& mat, const fvector& in, const fvector& out){
	//forward pass
	a.in().Copy(in);
	a.run(mat, &z);
	//now a and z calculated, calculate first layer's delta
	int L = mat.Matrix.Count();
	fvector& aL = a.Field[L];
	fvector& zL = z.Field[L];
	fvector& dL = delta.Field[L];
	int nL = nL = dL.Count();
	for (int i = 0; i < nL; i++){
		float dcda = aL[i] - out[i];
		dcda *= SigmaDerivative(zL[i]);
		dL[i] = dcda;
	}
	//prpagating back, toward the first layer
	for (int l = L - 1; l > 0; l--){
		fvector& al = a.Field[l];
		fvector& zl = z.Field[l];
		fvector& dl = delta.Field[l];
		fvector& dl1 = delta.Field[l + 1];
		int nl1 = dl1.Count();
		int nl = dl.Count();
		mat.Matrix[l].MulT(dl1,dl);
		for (int i = 0; i < nl; i++){
			dl[i] *= SigmaDerivative(zl[i]);
		}
	}
}
void TeachingEpoch::Create(const PerceptronsLayers& mat, int NSamples){
	PerceptronTeacher t;
	Epoch.SetCount(NSamples, t);
	for (int i = 0; i < NSamples; i++){
		Epoch[i].CreateFields(mat);
	}
}
void TeachingEpoch::Teach(PerceptronsLayers& mat, const SamplesLibrary& Lib, float step){
	if (Test.Field.Count() == 0){
		mat.CreateField(Test);
	}
	tbb_for(int i, Epoch.Count()){
		int rv = rand() + GetTickCount();
		int p = rv % Lib.inSamples.Count();
		if (p < 0)p = -p;
		if (p >= Lib.inSamples.Count())p = 0;
		Epoch[i].Backpropagation(mat, Lib.inSamples[p], Lib.outSamples[p]);
		Epoch[i].sampIdx = p;
	}tbb_for_end;
	for (int i = 0; i < Epoch.Count(); i++){
		PerceptronTeacher& t = Epoch[i];
		for (int k = 0; k < mat.Matrix.Count(); k++){
			PerceptronLayer& pl = mat.Matrix[k];
			const float* delta = t.delta.Field[k + 1].ToPtr();
			float* weights = pl.WeightsMatrix.ToPtr();
			float* ak0 = t.a.Field[k].ToPtr();
			float* bias = pl.Biases.ToPtr();
			for (int p = 0; p < pl.NPerceptrons; p++){
				float d = *(delta++);
				float* a = ak0;
				d *= step;
				*(bias++) -= d;
				for (int q = 0; q < pl.NInput; q++){
					*(weights++) -= *(a++) * d;
				}
			}
		}
	}
}
void PerceptronsLayers::AddLayer(int Num, bool Random){
	PerceptronLayer L;
	int nin = NInput;
	if (Matrix.Count())nin = Matrix.GetLast().NPerceptrons;
	Matrix.Add(L);
	Matrix.GetLast().Create(nin, Num, Random);
}
void PerceptronsLayers::CreateField(PerceptronField& field, bool SkipFirst) const{
	fvector f;
	field.Field.SetCount(Matrix.Count() + 1, f);
	for (int i = SkipFirst ? 1 : 0 ; i <= Matrix.Count(); i++){
		float v = 0;
		field.Field[i].SetCount(i > 0 ? Matrix[i - 1].NPerceptrons : NInput, v);
	}
}
fvector& PerceptronField::in(){
	return Field[0];
}
fvector& PerceptronField::out(){
	return Field.GetLast();
}
void PerceptronField::run(const PerceptronsLayers& mat, PerceptronField* a){
	for (int i = 1; i < Field.Count(); i++){
		mat.Matrix[i - 1].Calculate(Field[i - 1], Field[i], a ? &a->Field[i] : NULL);
	}
}
int indian(int p){
	char* pc = (char*)&p;
	std::swap(pc[0], pc[3]);
	std::swap(pc[1], pc[2]);
	return p;
}
void SamplesLibrary::LoadTrainingSet(const char* img_name, const char* index_name){
	FILE* Fim = NULL;
	fopen_s(&Fim, img_name, "rb");
	FILE* Fidx = NULL;
	fopen_s(&Fidx, index_name, "rb");
	if (Fim && Fidx){
		DWORD mg;
		fread(&mg, 4, 1, Fim);
		fread(&mg, 4, 1, Fidx);
		int ne1, ne2;
		fread(&ne1, 4, 1, Fim);
		fread(&ne2, 4, 1, Fidx);
		int szx, szy;
		fread(&szx, 4, 1, Fim);
		fread(&szy, 4, 1, Fim);
		szx = indian(szx);
		szy = indian(szy);
		ne1 = indian(ne1);
		ne2 = indian(ne2);
		if (ne1 == ne2){
			int sz = szx*szy;
			fvector f;
			inSamples.SetCount(ne1, f);
			outSamples.SetCount(ne1, f);
			unsigned char* refbuf = new unsigned char[ne1];
			unsigned char* imbuf = new unsigned char[ne1*sz];
			fread(imbuf, 1, ne1*sz, Fim);
			fread(refbuf, 1, ne1, Fidx);
			int ofs = 0;
			for (int i = 0; i < ne1; i++){
				float v = 0;
				inSamples[i].SetCount(sz, v);
				outSamples[i].SetCount(10, v);
				outSamples[i][refbuf[i]] = 1.0;
				for (int k = 0; k < sz; k++){
					inSamples[i][k] = imbuf[k + ofs] / 255.0f;
				}
				ofs += sz;
			}
			delete[]imbuf;
			delete[]refbuf;
		}
		if (Fim)fclose(Fim);
		if (Fidx)fclose(Fidx);
	}
}
float SamplesLibrary::Teach(PerceptronsLayers& mat, int NumInChunk, int NumIterations){
	return 0;
}
float SamplesLibrary::Test(PerceptronsLayers& mat){
	tbb::atomic<int> ns;
	ns.fetch_and_store(0);
	cList<tbb::internal::tbb_thread_v3::id> threads;
	cList<PerceptronField*> fields;
	tbb::spin_mutex sm;
	tbb_for(int i, inSamples.Count()){
		PerceptronField* fld = NULL;
		tbb::internal::tbb_thread_v3::id thread = tbb::this_tbb_thread::get_id();
		tbb::spin_mutex::scoped_lock mlock(sm);
		for (int k = 0; k < threads.Count(); k++)if (threads[k] == thread){
			fld = fields[k];
			break;
		}
		if (!fld){
			threads.Add(thread);
			fields.Add(new PerceptronField);
			mat.CreateField(*fields.GetLast());
			fld = fields.GetLast();
		}
		mlock.release();
		fld->in().Copy(inSamples[i]);
		fld->run(mat);
		fvector& outp = fld->out();
		float maxa1 = 0;
		float maxa2 = 0;
		int idx1 = 0;
		int idx2 = 0;
		for (int k = 0; k < outp.Count(); k++){
			if (outp[k] > maxa1){
				maxa1 = outp[k];
				idx1 = k;
			}
			float f1 = outSamples[i][k];
			if (f1 > maxa2){
				maxa2 = f1;
				idx2 = k;
			}
		}
		if (idx1 == idx2){
			ns.fetch_and_add(1);
		}
	}tbb_for_end;
	for (int i = 0; i < fields.Count(); i++){
		delete(fields[i]);
	}
	return float(ns) / float(inSamples.Count());
}
float SamplesLibrary::CalcSquaredDifference(PerceptronsLayers& mat) const{
	float s = 0;
	cList<tbb::internal::tbb_thread_v3::id> threads;
	cList<PerceptronField*> fields;
	tbb::spin_mutex sm;
	tbb::spin_mutex sm1;
	tbb_for(int i, inSamples.Count()){
		PerceptronField* fld = NULL;
		tbb::internal::tbb_thread_v3::id thread = tbb::this_tbb_thread::get_id();
		tbb::spin_mutex::scoped_lock mlock(sm);
		for (int k = 0; k < threads.Count(); k++)if (threads[k] == thread){
			fld = fields[k];
			break;
		}
		if (!fld){
			threads.Add(thread);
			fields.Add(new PerceptronField);
			mat.CreateField(*fields.GetLast());
			fld = fields.GetLast();
		}
		mlock.release();
		fld->in().Copy(inSamples[i]);
		fld->run(mat);
		fvector& outp = fld->out();
		float ds = 0;
		for (int k = 0; k < outp.Count(); k++){
			float d = outp[k] - outSamples[i][k];
			ds += d*d;
		}
		tbb::spin_mutex::scoped_lock mlock1(sm1);
		s += ds;
	}tbb_for_end;
	for (int i = 0; i < fields.Count(); i++){
		delete(fields[i]);
	}
	s /= float(inSamples.Count())*2;
	return s;
}
float SamplesLibrary::CalcSquaredDifference(int SampleIndex, PerceptronsLayers& mat, PerceptronField& PF) const{
	float s = 0;
	PF.in().Copy(inSamples[SampleIndex]);
	PF.run(mat);
	fvector& outp = PF.out();
	for (int k = 0; k < outp.Count(); k++){
		float d = outp[k] - outSamples[SampleIndex][k];
		s += d*d;
	}
	return s;
}
char* ch = " .o0";
void SamplesLibrary::Print(int idx){
	for (int i = 0; i < 28; i++){
		for (int j = 0; j < 28; j++){
			float f = inSamples[idx][j + i * 28];
			int k = int(f * 5);
			if (k > 3)k = 3;
			printf("%c", ch[k]);
		}
		printf("\n");
	}
	for (int k = 0; k < 10; k++){
		if (outSamples[idx][k] > 0.9)printf("Character: %d\n\n", k);
	}
}
int main(int argc, char* argv[])
{	
	SamplesLibrary SL;
	SL.LoadTrainingSet("data\\train-images.idx3-ubyte", "data\\train-labels.idx1-ubyte");

	PerceptronsLayers PM(28*28);
	//PM.AddLayer(80, true);
	PM.AddLayer(30, true);
	PM.AddLayer(10, true);

	PerceptronField PF;
	PM.CreateField(PF);
	
	//float c = SL.CalcSquaredDifference(PM, PF);

	TeachingEpoch E;
	E.Create(PM, 32);
	int ns = 1000000;
	for (int i = 0; i < ns; i++){
		E.Teach(PM, SL, 0.1f*ns/(ns+i*2));
		if ((i % 200) == 199){
			printf("%d%% (%d passes)\nsuccess: %f\n", i * 100 / ns, i*32, SL.Test(PM));
		}
	}

	//float c1 = SL.CalcSquaredDifference(PM, PF);

	//float r = SL.Test(PM);
	//float c = SL.CalcSquaredDifference(PM);

	//printf("%f,  %f\n", r, c);
	return 0;
}

