#pragma once

#include "comms-Math/comms-Math.h"
using namespace std;
using namespace comms;

float Sigma(float x);
float SigmaDerivative(float x);
//vector of floats
typedef cList<float> fvector;

//dotproduct of two floating vectors
_inline float fvdot(const fvector& v1, const fvector& v2){
	const float* p1 = v1.ToPtr();
	const float* p2 = v2.ToPtr();
	int n = min(v1.Count(), v2.Count());
	float s = 0;
	for (int i = 0; i < n; i++)s += p1[i] * p2[i];
	return s;
}

//v1 += v2;
_inline void fvinc(fvector& v1, const fvector& v2){
	float* p1 = v1.ToPtr();
	const float* p2 = v2.ToPtr();
	int n = v1.Count();
	for (int i = 0; i < n; i++){
		*(p1++) += *(p2++);
	}
}
//layer of perceptrons. Weights are organized as matrix NPerceptrons*NInput
//Each perceptron has NInput entrances
class PerceptronLayer{
public:
	
	fvector WeightsMatrix;//Weights, has dimension NPerceptrons*NInput (rows*columns)
	fvector Biases;//Bias for each perceptron, has dimension NPerceptrons
	int NPerceptrons;//Amount of perceptrons in the layer
	int NInput;//Amount of input connections to the layer. Should be same as count of output branches from previous layer


	void Mul(const fvector& in, fvector& out) const;//in has NInput dimension, out - NPerceptrons
	void MulT(const fvector& in, fvector& out) const;//in has NPerceptrons dimension, out - NInput
	void MulSig(const fvector& in, fvector& out, fvector* z) const;//multiply and apply Sigma, in has NInput dimension, out - NPerceptrons

	void Create(int NInput, int NPerceptrons,bool Random);//create and initialize weights and bias. Should run it before any calculations
	void Calculate(fvector& in, fvector& out, fvector* z = NULL) const;//Forward pass through layer. Optionally calculates values z[i] before applying Sigma
	void random();
};

class PerceptronField;
class PerceptronTeacher;
class SamplesLibrary;

//whole set of layers
class PerceptronsLayers{
	int NInput;//Number of input connections
	cList<PerceptronLayer> Matrix;//set of PerceptronLayer - s

	friend class PerceptronTeacher;
	friend class TeachingEpoch;
	friend class PerceptronField;

public:
	PerceptronsLayers();
	PerceptronsLayers(int NumberOfInputConnections);

	void random();//random inialisation
	void AddLayer(int Num, bool Random);//add new layer of perceptrons
	void CreateField(PerceptronField& field, bool SkipFirst = false) const;
};
class PerceptronField{
public:
	cList<fvector> Field;
	fvector& in();//place input data there
	fvector& out();//get output there after runnong run(...)
	void run(const PerceptronsLayers& mat, PerceptronField* z = NULL);//fill in(), run, get out()
};

//performing back propagation
class PerceptronTeacher{
public:
	int sampIdx;
	PerceptronField a;
	PerceptronField z;
	PerceptronField delta;
	void CreateFields(const PerceptronsLayers& mat);
	void Backpropagation(const PerceptronsLayers& mat, const fvector& in, const fvector& out);
};
//teaching the PerceptronsLayers using training with random samples
class TeachingEpoch{
public:
	PerceptronField Test;
	cList<PerceptronTeacher> Epoch;
	void Create(const PerceptronsLayers& mat,int NSamples);//first, you should create TeachingEpoch, NSamples is amount of samples for simultaneous teaching. NSamples should be proportional to CPU cores count for better performance
	void Teach(PerceptronsLayers& mat, const SamplesLibrary& Lib, float step);//one step of teaching the PerceptronsLayers. 
};
//Samples for training and checking the PerceptronsLayers
class SamplesLibrary{
public:
	cList<fvector> inSamples;
	cList<fvector> outSamples;
	void LoadTrainingSet(const char* img_name, const char* index_name);//look http://yann.lecun.com/exdb/mnist/ for downloading data set and data format specification
	float Teach(PerceptronsLayers& mat,int NumInChunk, int NumIterations);
	float Test(PerceptronsLayers& mat);//gives 0..1 result of checking
	float TestSample(int SampleIdx, PerceptronsLayers& mat, PerceptronField& PF);
	float CalcSquaredDifference(PerceptronsLayers& mat) const ;
	float CalcSquaredDifference(int SampleIdx, PerceptronsLayers& mat, PerceptronField& PF) const ;
	void Print(int idx);
};